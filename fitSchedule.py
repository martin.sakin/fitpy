#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Martin Sakin
# Date: September, 2017
#
# This script shows day, time and room of lectures.
# Edit setting in EDIT SECTION or just run for a example:
# 	./fitSchedule.py zzn tin bio sfc

import os, sys
import urllib
import urllib.request
from lxml import etree
# sudo -E python `which pip` install lxml --upgrade

#==============================================================================
#============================== EDIT SECTION ==================================
lectures = "zzn sfc bio"
shortPrint = True
LOG = True		# progress bar
#==============================================================================
#==============================================================================

lectures = lectures.upper().split()
WCOURSES = "http://www.fit.vutbr.cz/study/course-l.php.cs"
WBASE = "http://www.fit.vutbr.cz"
database = {}


def wget(page):
	try:
		req = urllib.request.urlopen(page, timeout = 3)
	except:
		print("No connection or wget problem")
		exit(1)

	charset = req.info().get_content_charset()
	html = req.read().decode(charset)
	return html


def printByLectures():
	for abbr, data in sorted(database.items()):
		try:
			if shortPrint:
				last = {
						'start': '',
						'room': '',
						'day': ''
					}
				for slot in database[abbr]['slots']:
					if slot['day'] == last['day'] and slot['start'] == last['start'] and slot['room'] != last['room']:
						print(slot['room'], '', end="")
					else:
						if not last['day'] == '':
							print()
						print(abbr, slot['day'], slot['start'] +'-'+ slot['end'], slot['type'], slot['room'], '', end="")
					last = slot
				print(end='\n')

			else:
				print(abbr, '-', data['title'])
				last = {
						'start': '',
						'room': '',
						'day': ''
					}
				for slot in database[abbr]['slots']:
					if slot['day'] == last['day'] and slot['start'] == last['start'] and slot['room'] != last['room']:
						print(slot['room'], '', end="")
					else:
						if not last['day'] == '':
							print()
						print(slot['day'], slot['start'] +'-'+ slot['end'], slot['type'], slot['room'], '', end="")
					last = slot
				print(end='\n\n')

		except:
			print('! missing data for', abbr)


def main():
	global database
	for abbr in lectures:
		database[abbr] = {}

	html = wget(WCOURSES)
	tree = etree.HTML(html)

	node = tree.xpath('//*[@id="contenttable"]//form/table/tr')

	for iii in range(3, len(node)+1):
		'//*[@id="contenttable"]/tbody/tr/td/form/table/tbody/tr[3]/td[2]/b'
		abbr = tree.xpath('//*[@id="contenttable"]//form/table/tr['+str(iii)+']/td[2]/b')

		if abbr[0].text in lectures:
			title = tree.xpath('//*[@id="contenttable"]//form/table/tr['+str(iii)+']/td[1]/a')
			database[abbr[0].text]['title'] = title[0].text

			link = tree.xpath('//*[@id="contenttable"]//form/table/tr['+str(iii)+']/td[1]/a/@href')
			database[abbr[0].text]['link'] = link[0]

			database[abbr[0].text]['slots'] = []

	for abbr, attributes in database.items():
		try:
			if LOG:
				print("getting", abbr, end="")
				sys.stdout.flush()
			html = wget(WBASE+attributes['link'])
		except:
			if LOG:
				print("\nUnknown:", abbr)
			else:
				print("Unknown:", abbr)
			continue

		tree = etree.HTML(html)

		node = tree.xpath('//*[@id="contenttable"]/tr/td/table/tr/td/table[1]/tr')	# num of lines in tables
		lines = len(node)+2

		node = tree.xpath('//*[@id="contenttable"]/tr/td/table/tr/td/table/tr/th')	# day
		colomns = len(node)+2

		for lll in range(0, lines):
			if LOG and lll < 68:	# dots
				print('.', end="")
				sys.stdout.flush()

			for ccc in range(0, colomns):
				rrr = tree.xpath('//*[@id="contenttable"]/tr/td/table/tr['+str(ccc)+']/td/table/tr['+str(lll)+']/td')

				if len(rrr) != 8:	# magic number (colomns)
					continue

				if rrr[0].text in ['přednáška', 'lecture', 'cvičení', 'exercise']:
					room = tree.xpath('//*[@id="contenttable"]/tr/td/table/tr['+str(ccc)+']/td/table/tr['+str(lll)+']/td[3]/a')
					day = tree.xpath('//*[@id="contenttable"]/tr/td/table/tr['+str(ccc)+']/td/table/tr['+str(lll)+']/th')

					slot = {
						'type': rrr[0].text,
						'start': rrr[3].text,
						'end': rrr[4].text,
						'room': room[0].text,
						'day': day[0].text
					}

					if not slot in database[abbr]['slots']:
						database[abbr]['slots'].append(slot)
		if LOG:
			print('\r                                                                               \r', end="")
			sys.stdout.flush()

	printByLectures()


if __name__ == '__main__':
	if len(sys.argv) >= 2:
		lectures = []
		for i in range(1, len(sys.argv)):
			lectures.append(sys.argv[i].upper())
	main()

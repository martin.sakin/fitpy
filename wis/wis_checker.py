#!/usr/bin/python3
# Author: Martin Sakin, martin.sakin <at> gmail.com
# Date: 2017-10-08
# Topic: Checker for WIS
# This script check difference every "interval" time on page in "lecture".
# It will send you mail with diff, when some change.
# Best run on merlin in screen.
# $ nohup python3 -u ./checkuj.py &
# TODO: diff in python, wget in python
import sys, os
import subprocess
import time
import re
import getpass
import traceback
#============================= Settings =======================================
login = ""	# xlogin00
passwd= ""

lecture = {}
#lecture['Enrolled'] = 'wis.fit.vutbr.cz/FIT/st/study-v.php'
lecture['TIN'] = 'wis.fit.vutbr.cz/FIT/st/course-sl.php?id=619473'
lecture['MAT'] = 'wis.fit.vutbr.cz/FIT/st/course-sl.php?id=619471'
lecture['BIO'] = 'wis.fit.vutbr.cz/FIT/st/course-sl.php?id=654752'

interval = 60	# seconds
mailReceiver = ''	# email@example.com
LOG = False

#=============================== Code =========================================
def timeNow():
	return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())


def nameScript():
	return sys.argv[0]


def downloadPage(title, typ):
	command = ["wget", "--no-check-certificate",
		"-O", title + "." + typ,
		"https://" + login + ":" + passwd + "@" + lecture[title] ]

	process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = process.communicate()

	cleanHtml(title + '.' + typ)


def replaceQuations(content):
	content.strip()
	content = re.sub(r'&nbsp;', ' ', content)
	content = re.sub(r'"', "'", content)
	return content


def cleanHtml(namefile):	# this lines are variable, don't want it
	output = ""
	with open(namefile, 'r+', encoding="ISO-8859-1") as f:
		content = f.read()
		for line in content.splitlines():
			if line.find('var uri = ') > -1:
				pass
			elif line.find('<div id="clock">') > -1:
				pass
			elif line.find('name="token" value=') > -1:
				pass
			else:
				output += line + '\n'

		f.seek(0)
		f.write(output)


def sendMail(subject, content):
	if mailReceiver:
		command = str('echo "' + content
			+ '" | mail -s "$(echo -e "' + subject
			+ '\nContent-Type: text/html")" "' + mailReceiver + '"')
		os.system(command)


def makeDiff(title):
	command = ["diff", title+'.html', title+'.tmp']
	process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	return process.communicate()


def checkLoginData():
	global login, passwd

	if not login:
		login = input("Login to WIS: ")

	if not passwd:
		passwd = getpass.getpass("Password for '" + login + "' to WIS: ")

	command = ["wget", "--no-check-certificate",
		"-OtextConnectionFile.tmp",
		"https://" + login + ":" + passwd + "@wis.fit.vutbr.cz/FIT/st/study-v.php"]

	process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = process.communicate()

	os.remove("textConnectionFile.tmp")

	return err.decode('utf-8').find("200 OK") > -1


def main():
	if checkLoginData():
		if LOG: print("LOGIN OK")
	else:
		print("LOGIN FAILED")
		exit(10)

	pidi = os.getpid()
	print('Start! PID: ' + str(pidi))

	sendMail("Script for checking WIS started.",
		"WIS checker run<br>\nStart time: " + timeNow()
		+ "<br>\nPID: " + str(pidi)
		+ "<br>\nLectures: " + ', '.join(sorted(lecture.keys())))

	if LOG: print("Download reference of pages:", list(lecture.keys()))
	for title in lecture:
		downloadPage(title, 'html')

	while True:
		for title in lecture:
			if LOG: print('WGET ' + title + ': ' + timeNow())
			downloadPage(title, 'tmp')

			output, err = makeDiff(title)

			output = output.decode('ISO-8859-1')

			if LOG and output:
				print('Diff:\n' + output)

			if not output and not err:	# no change
				if LOG: print('No change')
			else:
				if LOG: print('Change !!!')
				if err:
					output += err.decode('ISO-8859-1')

				output = replaceQuations(output)
				output = str('<html>Change in ' + title + '<br>\n'
					+ timeNow() + '<br>\n<br>\n' + output + '</html>')
				sendMail("Change in " + title, output)

				os.rename(title+'.tmp', title+'.html')	# update reference file

			try: os.remove(title+'.tmp')
			except: pass

		if LOG: print('===== Sleep for ' + str(interval) + ' seconds =====')
		time.sleep(interval)


if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		print(" Script ended")
		sendMail("Script ended", "Script " + nameScript() + " was ended at " + timeNow() + "<br>\n")
	except:
		errType = traceback.format_exc()
		print(errType)

		sendMail("Script failed", "Fail in script " + nameScript() + '<br>\n'
			+ "Date: " + timeNow() + "<br>\n" + "Error type: " + errType)

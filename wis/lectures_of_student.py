#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date : 2019-11-12
# @Author : Martin Sakin, martin.sakin <at> gmail.com
# Get student lectures by xlogin.

ILOGIN = '' # ilogin

import os
import sys
import time
import getpass
import subprocess
import tempfile
import random
import re
from lxml import etree # sudo -H pip3 install lxml


class Students_studium():
	def __init__(self, xlogin):
		self.lectures = []
		self.login = xlogin
		self.name = ""

	def add_name(self, name):
		self.name = name

	def add_lecture(self, ide, shortcut, lecture_name, points, grade):
		self.lectures.append({
			'ide' : ide,
			'shortcut' : shortcut,
			'name' : lecture_name,
			'points' : points,
			'grade' : grade
		})

	def print(self):
		print(self.name + " (" + self.login + ")")

		for ii in self.lectures:
			if ii:
				print(ii['shortcut'] +' | '+ ii['name'] + '  || ' + ii['points'] +' | '+ ii['grade'] + ' | ')


class WIS(object):
	""" employment wis """
	def __init__(self, user, year, semester):
		self.user = user
		self.passwd = ""
		self.year = year
		self.semester = semester
		self.lectures = []

		if not self.user:
			self.user = input("ilogin to WIS: ")
		if os.path.isfile(os.path.expanduser("~/.wis")):
			with open(os.path.expanduser("~/.wis")) as f:
				self.passwd = f.read().strip()
		if not self.passwd:
			self.passwd = getpass.getpass("Password for '" + self.user + "' to WIS: ")

	def fetch_lectures(self):
		#url0 = "wis.fit.vutbr.cz/FIT/db/vyuka/ucitel/course.php.cs?submit_reset=Impl.+filtr&limit=1000&order=2&form_f1=" + self.year + "&form_f5=" + self.semester
		#self.wget(url0)  # for reset view
		url = "wis.fit.vutbr.cz/FIT/db/vyuka/ucitel/course.php.cs?submit_view=t&limit=1000&order=2&form_f1=" + self.year + "&form_f5=" + self.semester
		namefile = self.wget(url)
		with open(namefile, 'r+', encoding="ISO-8859-2") as f:
			for line in f:
				if line.find('class="dataview0"') > -1 or line.find('class="dataview1"') > -1:
					self.lectures.append(self.parse_dataview_line(line))
		os.remove(namefile)

	def parse_dataview_line(self, line):
		lecture = {}
		parts = line.replace("<td","").replace("</td>","\n").split("\n")
		for idx, part in enumerate(parts):
			if part.find(self.year) > -1:
				lecture['shortcut'] = parts[idx+1].split(">")[1]
				lecture['id'], lecture['name'] = self.parse_dataview_link(parts[idx+3]) # + link name of lecture
				return lecture

	def parse_dataview_link(self, line):
		txt = line.split("?")[1]
		ide = re.search(r'\d+', txt).group(0)
		link = re.compile('">(.*?)</a>').search(txt).group(1)
		return ide, link

	def wget(self, url):
		filename = "/tmp/pywistmp." + str(random.randint(1000,99999)) + ".delete"
		command = ["wget", "--no-check-certificate",
					"-O", filename,
					"https://" + self.user + ":" + self.passwd + "@" + url ]
		process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = process.communicate()
		time.sleep(0.1)

		return filename

	def fetch_student(self, xlogin):
		studium = Students_studium(xlogin)

		for lecture in self.lectures:
			url = "wis.fit.vutbr.cz/FIT/db/vyuka/ucitel/course-s.php.cs?id="+lecture['id']+"&order=2&limit=1000&form_f1="+self.year+"&form_f3="+self.semester

			namefile = self.wget(url)
			with open(namefile, 'r+', encoding="ISO-8859-2") as f:
				content = f.read()
				tree = etree.HTML(str(content))

				node = tree.xpath('/html/body/table[4]/tr[2]/td/table/tr[2]/td/table/tr')
				num_of_people = len(node)
				if num_of_people <= 1:	# no student here
					continue

				for ii in range(2, num_of_people):		# students in lecture
					node = tree.xpath('/html/body/table[4]/tr[2]/td/table/tr[2]/td/table/tr['+str(ii)+']/td[3]/a')
					if len(node) < 1:	# no student here
						continue

					xstud = node[0].text
					if xstud == xlogin:
						node = tree.xpath('/html/body/table[4]/tr[2]/td/table/tr[2]/td/table/tr['+str(ii)+']/td[2]/a')
						name = node[0].text
						studium.add_name(name)

						node = tree.xpath('/html/body/table[4]/tr[2]/td/table/tr[2]/td/table/tr['+str(ii)+']/td[13]')
						points = node[0].text

						node = tree.xpath('/html/body/table[4]/tr[2]/td/table/tr[2]/td/table/tr['+str(ii)+']/td[14]')
						grade = node[0].text

						studium.add_lecture(lecture['id'], lecture['shortcut'], lecture['name'], points, grade)

			os.remove(namefile)

		return studium


def main(student, year, semester):
	wis = WIS(ILOGIN, year, semester)
	wis.fetch_lectures()
	studium = wis.fetch_student(student)
	studium.print()


if __name__ == '__main__':
	if len(sys.argv) != 4:
		print("Help: add login as parametr.")
		print(" $", sys.argv[0], "xloginNN year Z/L")
	else:
		main(sys.argv[1], sys.argv[2], sys.argv[3])

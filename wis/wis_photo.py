#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date : 2019-11-12
# @Author : Martin Sakin, martin.sakin <at> gmail.com
# Show photo of student by xlogin.
# Searching in IZP and TIN

ILOGIN = '' # ilogin

import os
import sys
import time
import datetime
import getpass
import subprocess
import tempfile
import random
import re
import urllib.request
from lxml import etree # sudo -H pip3 install lxml

HISTORY_YEARS = 6

class WIS():
	""" employment wis """
	def __init__(self, user):
		self.user = user
		self.passwd = ""
		self.yearNow = datetime.datetime.now().year
		self.studentLogin = ""

		if not self.user:
			self.user = input("ilogin to WIS: ")
		if os.path.isfile(os.path.expanduser("~/.wis")):
			with open(os.path.expanduser("~/.wis")) as f:
				self.passwd = f.read().strip()
		if not self.passwd:
			self.passwd = getpass.getpass("Password for '" + self.user + "' to WIS: ")


	def wget(self, url):
		cmd = "wget --no-check-certificate -O /dev/stdout 2> /dev/null 'https://" + self.user + ":" + self.passwd + "@" + url + "'"
		out = subprocess.check_output(cmd, stderr=subprocess.DEVNULL, shell=True).decode("ISO-8859-2")
		time.sleep(0.5)
		return out


	def goThrough(self, courseShortcut):
		for year in range(self.yearNow, self.yearNow-HISTORY_YEARS, -1):
			urlLectures = "wis.fit.vutbr.cz/FIT/db/vyuka/ucitel/course.php.cs?submit_view=t&limit=1000&order=2&form_f1="+str(year)+"&form_f2="+courseShortcut+"&&form_f5=Z"
			content = self.wget(urlLectures)

			tree = etree.HTML(content)
			node = tree.xpath('/html/body/table[3]/tr[1]/td/form/table[2]/tr[4]/td[5]/a') # path to course link
			linkToCourse = node[0].items()[0][1]
			idCourse = linkToCourse.split("?")[1].split("&")[0].split("=")[1]

			# Get list of students in course and compare login of every student
			if self.searchStudent(idCourse, courseShortcut, year):
				return True

		print(" - Nothing in", courseShortcut, "in years", self.yearNow-HISTORY_YEARS+1, "-", self.yearNow)
		return False


	def findStudent(self, studLogin):
		self.studentLogin = studLogin

		# Search in IZP lectures, years decreasing
		if self.goThrough("IZP"):
			return

		# Search in TIN lectures, years decreasing
		if self.goThrough("TIN"):
			return


	def searchStudent(self, idCourse, courseShortcut, year):
		urlStudents = "wis.fit.vutbr.cz/FIT/db/vyuka/ucitel/course-s.php.cs?id=" + idCourse
		content = self.wget(urlStudents)

		tree = etree.HTML(content)
		node = tree.xpath('/html/body/table[4]/tr[2]/td/table/tr[2]/td/table/tr') # paths to students

		# Compare login of every student
		for i in range(2,len(node)-1):
			login = node[i].xpath('td[3]/a')[0].text
			if login == self.studentLogin:
				studLink = node[i].xpath('td[2]/a')[0].items()[0][1]
				name = node[i].xpath('td[2]/a')[0].text
				email = node[i].xpath('td[3]/a')[0].items()[0][1].split(":")[1]
				print("===", courseShortcut, "===", year, "===")
				print("Name: ", name)
				print("Email:", email)
				idPhoto = re.search(r'pid=\d+', studLink).group(0).split("=")[1]
				print("Photo:", "https://wis.fit.vutbr.cz/FIT/db/hr/view_person_photo_cdb.php?id="+idPhoto)
				return True

		return False




def main(student):
	wis = WIS(ILOGIN)
	wis.findStudent(student)


if __name__ == '__main__':
	if len(sys.argv) == 2:
		main(sys.argv[1])
	else:
		print("Help: add login as parametr.")
		print(" $", sys.argv[0], "xloginNN")
